import json

class Expend_jconf(object):

	def __init__(self):
		self.location = ""
		self.amount = 0
		self.date = []

	def parse(self, json_string):
		result_info = []
		parsed_json = json.loads(json_string)

		if(type(parsed_json["location"]) == str):
			self.location = parsed_json["location"]
		else:
			result_info.append("Incorrect type: location")

		if(type(parsed_json["amount"]) == int):
			self.amount = parsed_json["amount"]
		else:
			result_info.append("Incorrect type: amount")

		if(type(parsed_json["date"]) == list):
			for each in parsed_json["date"]:
				if(type(each) == int):
					self.date.append(each)
				else:
					result_info.append("Incorrect type in list: date")
		else:
			result_info.append("Incorrect type: date")

		return result_info
	def unparse(self):
		myself = {}

		myself["location"] = self.location
		myself["amount"] = self.amount
		myself["date"] = self.date

		return json.dumps(myself, indent = 3)
