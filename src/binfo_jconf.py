import json

class Binfo_jconf(object):

	def __init__(self):
		self.total = 0
		self.finished = True

	def parse(self, json_string):
		result_info = []
		parsed_json = json.loads(json_string)

		if(type(parsed_json["total"]) == int):
			self.total = parsed_json["total"]
		else:
			result_info.append("Incorrect type: total")

		if(type(parsed_json["finished"]) == bool):
			self.finished = parsed_json["finished"]
		else:
			result_info.append("Incorrect type: finished")

		return result_info
	def unparse(self):
		myself = {}

		myself["total"] = self.total
		myself["finished"] = self.finished

		return json.dumps(myself, indent = 3)
