import json
from binfo_jconf import Binfo_jconf
from categories_jconf import Categories_jconf

class Budget_jconf(object):

	def __init__(self):
		self.binfo = Binfo_jconf()
		self.categories = []

	def parse(self, json_string):
		result_info = []
		parsed_json = json.loads(json_string)

		if(type(parsed_json["binfo"]) == dict):
			result_info.extend(self.binfo.parse(json.dumps(parsed_json["binfo"])))
		else:
			result_info.append("Incorrect type: binfo")

		if(type(parsed_json["categories"]) == list):
			for each in parsed_json["categories"]:
				if(type(each) == dict):
					new_categories = Categories_jconf()
					result_info.extend(new_categories.parse(json.dumps(each)))
					self.categories.append(new_categories)
				else:
					result_info.append("Incorrect type in list: categories")
		else:
			result_info.append("Incorrect type: categories")

		return result_info
	def unparse(self):
		myself = {}

		myself["binfo"] = json.loads(self.binfo.unparse())
		myself["categories"] = []
		for each in self.categories:
			myself["categories"].append(json.loads(each.unparse()))

		return json.dumps(myself, indent = 3)
