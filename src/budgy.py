import tkinter as tk
import calendar
from budget_jconf import Budget_jconf
from expend_jconf import Expend_jconf

class App(tk.Frame):

	def __init__(self, parent, *args, **kwargs):
		self.bud = None
		
		tk.Frame.__init__(self, parent, *args,  **kwargs)
		self.parent = parent
		
		self.instruction_label = tk.Label(self, text='Choose your month and year')
		self.instruction_label.pack(side=tk.TOP, fill=tk.Y)
		
		self.month_listBox = tk.Listbox(self)
		for month in calendar.month_name:
			self.month_listBox.insert(tk.END, month)
		self.month_listBox.delete(0)
		self.month_listBox.pack(side=tk.LEFT)

		self.year_listBox = tk.Listbox(self)
		self.year_listBox.insert(tk.END, "2017")
		self.year_listBox.pack(side=tk.LEFT)

		self.output_text = tk.Text(self)
		self.output_text.pack(side=tk.RIGHT)
		
		self.add_frame = tk.Frame(self)
		self.add_frame.pack(side=tk.BOTTOM)
		
		self.category_entry = tk.Entry(self.add_frame, text="Category")
		self.category_entry.pack(side=tk.TOP)
		
		self.amount_entry = tk.Entry(self.add_frame, text="Amount")
		self.amount_entry.pack(side=tk.TOP)
		
		self.date_entry = tk.Entry(self.add_frame, text="Date")
		self.date_entry.pack(side=tk.TOP)
		
		self.description_entry = tk.Entry(self.add_frame, text="Description")
		self.description_entry.pack(side=tk.TOP)
		
		self.add_button = tk.Button(self, text="Add purchase", command=self._add_purchase)
		self.add_button.pack(side=tk.LEFT)

		self.select_button = tk.Button(self, text="select", command=self._retrieve_json)
		self.select_button.pack(side=tk.BOTTOM,padx=5,pady=5)
	
	def _retrieve_json(self):
		if not (self.output_text.get("1.0", tk.END) == "\n"):
			self.output_text.delete("1.0", tk.END)
		try:
			with open("../budgets/" + self.month_listBox.get(tk.ACTIVE) + "_" + self.year_listBox.get(tk.ACTIVE) +".json", 'r') as fh:
				self.bud = None
				self.bud = Budget_jconf()
				output = self.bud.parse(fh.read())
				if(output):
					self.output_text.insert(tk.INSERT, output)
				else:
					self.output_text.insert(tk.INSERT, self.bud.unparse())
		except:
			self.output_text.insert(tk.INSERT, "No information available for " + self.month_listBox.get(tk.ACTIVE) + " " + self.year_listBox.get(tk.ACTIVE))
		
	def _add_purchase(self):
		if self.bud:
			found_cat = False
			for cat in self.bud.categories:
				if cat.name == self.category_entry.get():
					new_entry = Expend_jconf()
					new_entry.location = self.description_entry.get()
					new_entry.amount = float(self.amount_entry.get())
					cat.expend.append(new_entry)
					found_cat = True
					break
			
			if not found_cat:
				self.output_text.delete("1.0", tk.END)
				self.output_text.insert(tk.INSERT, "Invalid category")
			else:
				with open("../budgets/" + self.month_listBox.get(tk.ACTIVE) + "_" + self.year_listBox.get(tk.ACTIVE) +".json", 'w') as fh:
					fh.write(self.bud.unparse())
		else:
			self.output_text.delete("1.0", tk.END)
			self.output_text.insert(tk.INSERT, "Choose month and year")
			
			
			
if __name__ == "__main__":
	root = tk.Tk()
	App(root).pack(side="top", fill="both", expand=True)
	root.mainloop()