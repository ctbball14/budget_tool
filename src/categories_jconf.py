import json
from expend_jconf import Expend_jconf

class Categories_jconf(object):

	def __init__(self):
		self.name = ""
		self.budget = 0
		self.expend = []

	def parse(self, json_string):
		result_info = []
		parsed_json = json.loads(json_string)

		if(type(parsed_json["name"]) == str):
			self.name = parsed_json["name"]
		else:
			result_info.append("Incorrect type: name")

		if(type(parsed_json["budget"]) == int):
			self.budget = parsed_json["budget"]
		else:
			result_info.append("Incorrect type: budget")

		if(type(parsed_json["expend"]) == list):
			for each in parsed_json["expend"]:
				if(type(each) == dict):
					new_expend = Expend_jconf()
					result_info.extend(new_expend.parse(json.dumps(each)))
					self.expend.append(new_expend)
				else:
					result_info.append("Incorrect type in list: expend")
		else:
			result_info.append("Incorrect type: expend")

		return result_info
	def unparse(self):
		myself = {}

		myself["name"] = self.name
		myself["budget"] = self.budget
		myself["expend"] = []
		for each in self.expend:
			myself["expend"].append(json.loads(each.unparse()))

		return json.dumps(myself, indent = 3)
